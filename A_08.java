public class A_08 {

    public static int[] reverseArr(int[] arr) {

        for (int i = 0; i < arr.length / 2; i++) {
            int temp = arr[arr.length - 1 - i];
            arr[arr.length - 1 - i] = arr[i];
            arr[i] = temp;
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        return arr;
    }

    public static void main(String[] args) {
        int arr[] = { 5, 6, 3, 5, 7, 8, 9, 1, 2 };
        reverseArr(arr);
    }

}
