public class A_07 {
    static boolean checkPair(int A[], int c) {
        for (int i = 0; i < A.length - 1; i++) {
            for (int j = i + 1; j < A.length; j++) {
                if (A[i] + A[j] == c) {
                    System.out.println("Pair with a given sum " + c + " is (" + A[i] + ", " + A[j] + ")");

                    return true;
                }
            }
        }

        return false;
    }

    public static void main(String[] args) {

        int A[] = { 0, 0, 3, 0, 1 };
        int c = 4;
        if (checkPair(A, c)) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }
}
